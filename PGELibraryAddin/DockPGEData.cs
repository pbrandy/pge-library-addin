﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
//using ArcDataBinding;
using FWARGTableWrapper;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Carto;
using Npgsql;
using ESRI.ArcGIS.esriSystem;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;

namespace PGELibraryAddin
{
    /// <summary>
    /// Designer class of the dockable window add-in. It contains user interfaces that
    /// make up the dockable window.
    /// </summary>
    public partial class DockPGEData : UserControl
    {
        BatchData _bd;// = new BatchData();
                      //IQueryFilter qf = new QueryFilterClass();
        GenericCode _workerBee = new GenericCode();
        bool _resourcesFiltered;
        bool _reportsFiltered;
        string _GISKey = "";
        private static readonly NLog.Logger logger = FWLogManager.Instance.GetCurrentClassLogger();
        public enum SettingsType
        {
            MPEResVisiblity,
            MPEResIndex,
            MPEResHeader,
            MPEPersonVisiblity,
            MPEPersonIndex,
            MPEPersonsHeader
        }
        public enum SettingsGroup
        {
            MPERes,
            MPEPerson
        }
        public enum columnTypes
        {
            checkbox,
            combo,
            text
        }
        public BatchData GetBatchData
        {
            get
            {
                return _bd;
            }
        }
        public DockPGEData(object hook)
        {

            InitializeComponent();
            _bd = new BatchData();
            Hook = hook;
            Binding bindNew = new Binding("Enabled", checkExistingVersion, "Checked");
            bindNew.Format +=
                (sender, e) =>
                    e.Value = !((bool)e.Value); // invert the checked value
            bindNew.Parse +=
                (sender, e) =>
                    e.Value = !((bool)e.Value); // invert the checked value
            Binding bindExisting = new Binding("Enabled", checkNewVersion, "Checked");
            bindExisting.Format +=
                    (sender, e) =>
                        e.Value = !((bool)e.Value); // invert the checked value
            bindExisting.Parse +=
                (sender, e) =>
                    e.Value = !((bool)e.Value); // invert the checked value
            checkNewVersion.DataBindings.Add(bindNew);
            checkExistingVersion.DataBindings.Add(bindExisting);


            comboBatchNames.DataBindings.Add("Enabled", checkExistingVersion, "Checked");
            buttonSetVersion.DataBindings.Add("Enabled", checkExistingVersion, "Checked");
            textNewBatchName.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            textComment.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            textPGENumber.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            textFWJobNumber.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            txtBatchPath.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            buttonCreateVersion.DataBindings.Add("Enabled", checkNewVersion, "Checked");
            Binding bind = new Binding("Enabled", chkReportMatchDisabled, "Checked");
            bind.Format += (sender, e) => e.Value = !((bool)e.Value);
            btnReportsUpdateMatch.DataBindings.Add(bind);

            pgeresourcesDataSetTableAdapters.lookupagencyTableAdapter lookupagencyTableAdapter = new pgeresourcesDataSetTableAdapters.lookupagencyTableAdapter();
            pgeresourcesDataSetTableAdapters.lookupeligibilityTableAdapter lookupeligibilityTableAdapter = new pgeresourcesDataSetTableAdapters.lookupeligibilityTableAdapter();
            pgeresourcesDataSetTableAdapters.lookupcountycodesTableAdapter lookupcountycodesTableAdapter = new pgeresourcesDataSetTableAdapters.lookupcountycodesTableAdapter();
            
            pgeresourcesDataSetTableAdapters.lookupreporttypeTableAdapter lookupreporttypeTableAdapter = new pgeresourcesDataSetTableAdapters.lookupreporttypeTableAdapter();
            lookupagencyTableAdapter.Fill(pgeresourcesDataSet.lookupagency);
            lookupeligibilityTableAdapter.Fill(pgeresourcesDataSet.lookupeligibility);
            lookupcountycodesTableAdapter.Fill(pgeresourcesDataSet.lookupcountycodes);
            lookupreporttypeTableAdapter.Fill(pgeresourcesDataSet.lookupreporttype);
            lookupmpeactivityTableAdapter.Fill(pgeresourcesDataSet.lookupmpeactivity);
            lookupSILTableAdapter.Fill(pgeresourcesDataSet.lookupSIL);

            EditableSettings es = new EditableSettings();
            cboGISLayer.DataSource = es.BatchLayerNames;
            FinishInitialize();


        }

        /// <summary>
        /// Host object of the dockable window
        /// </summary>
        private object Hook
        {
            get;
            set;
        }

        /// <summary>
        /// Implementation class of the dockable window add-in. It is responsible for 
        /// creating and disposing the user interface class of the dockable window.
        /// </summary>
        public class AddinImpl : ESRI.ArcGIS.Desktop.AddIns.DockableWindow
        {
            private DockPGEData m_windowUI;

            public AddinImpl()
            {
            }
            internal DockPGEData UI
            {
                get { return m_windowUI; }
            }
            protected override IntPtr OnCreateChild()
            {
                m_windowUI = new DockPGEData(Hook);

                return m_windowUI.Handle;
            }

            protected override void Dispose(bool disposing)
            {
                if (m_windowUI != null)
                    m_windowUI.Dispose(disposing);

                base.Dispose(disposing);
            }

        }

        private void FinishInitialize()
        {
            //_bd.Reinitialize();

            LoadBatch();
            //batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
        }
        public void LoadBatch()
        {
            _bd = new BatchData();
            _bd.Load();
            comboBatchNames.DataSource = _bd.GetVersions();
            //if (_bd.Enabled)
            //{
            tabControl1.Enabled = _bd.Enabled;
            //logger.Debug("Batches loaded {0} {1}", System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name,System.Reflection.MethodBase.GetCurrentMethod().Name);
            logger.Debug("Batches loaded {0} {1}", "DockPGEData", "LoadBatch");//System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name);


            //}
        }
        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            // if (base.Visible)
            // {
            //now visible
            if (!_bd.Enabled)
            {
                LoadBatch();
            }
            // }

        }
        private void batchResourceBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate && e.Exception == null)
                e.Binding.BindingManagerBase.EndCurrentEdit();
        }

        private void buttonLoadVersion_Click(object sender, EventArgs e)
        {


            //_bd.Load();
            //this.dataGridView1.DataSource = bd.BatchResourceRecords();
            //comboBatchNames.DataSource = _bd.GetVersions();
            //batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
        }

        private void fWProjectNumberTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void RefreshBatchData(bool reload)
        {
            _bd.SaveEdits(false);

            //reload if we haven't just created a new version or changed versioned
            if (reload) { _bd.LoadVersioned(); }

            //batchResourceBindingSource.DataSource = null;
            batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
            batchReportBindingSource.DataSource = _bd.BatchReportRecords();
           // mpereportBindingSource.DataSource = _bd.BatchMPEReportRecords("reportid = " + txtReportIDMPE.Text==""?"-9":this.txtReportIDMPE.Text);
           // mpereportBindingSource.DataSource = _bd.BatchMPEReportRecords();
            dgvMPEResource.DataSource = null;
           // mperesourceBindingSource.DataSource = _bd.BatchMPEResourceRecords();
           
            FeatureLayerList featureLayers = _workerBee.GetLayers(null, _bd.BatchWorkspace, null);
            IFeatureLayer fl;
            IFeatureLayerDefinition2 featDef;
            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "resourceid is null or resourceid in (" + _bd.BatchResourceGISWhere() + ")";
            }


            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "resourceid is null or resourceid in (" + _bd.BatchResourceGISWhere() + ")";
            }

            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "resourceid is null or resourceid in (" + _bd.BatchResourceGISWhere() + ")";
            }

            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "reportid is null or reportid in (" + _bd.BatchReportGISWhere() + ")";
            }


            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "reportid is null or reportid in (" + _bd.BatchReportGISWhere() + ")";
            }

            if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear) != -1)
            {
                fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear);
                featDef = (IFeatureLayerDefinition2)fl;
                featDef.DefinitionExpression = "reportid is null or reportid in (" + _bd.BatchReportGISWhere() + ")";
            }
            //ResourceBindingSource.DataSource = _bd.BatchResources();

        }
        private void buttonSetVersion_Click(object sender, EventArgs e)
        {
            _bd.ChangeVersion(this.comboBatchNames.Text);
            RefreshBatchData(false);
            LoadExistingBatch(_bd.VersionName().Replace(_bd.WorkspaceUser()+".",string.Empty));
            //tabControl1.SelectTab("tabPage2");
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }



        private void buttonImportSitePDFs_Click(object sender, EventArgs e)
        {
            _bd.ImportToBatch(BatchData.ImportType.ResourcePDF);
            //sitesDataGridView.DataSource = typeof(List<>);
            //sitesDataGridView.DataSource = batchResourceBindingSource;
            RefreshBatchData(false);


        }

        private void tabPage1_Click_1(object sender, EventArgs e)
        {

        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            comboBatchNames.DataSource = _bd.GetVersions();
        }

        //private void buttonTempWorkspace_Click(object sender, EventArgs e)
        //{

        //	PGELibraryAddin.Properties.Settings.Default.UserScratchWorkspace = _bd.SetTempWorkspace(textTempWorkspace.Text);
        //	PGELibraryAddin.Properties.Settings.Default.Save();
        //}

        private void batchResourceBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            //update list of potential matches
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //FWTableWrapper _VersionedResourceRecords = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table, null);
            batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
            drpSelection.Text = "";
            txtSearch.Text = string.Empty;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            //TableWrapperFW _VersionedResourceRecords = new TableWrapperFW(_workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table, null);
            //if (qf.WhereClause != "")
            //{
            //_VersionedResourceRecords = new TableWrapperFW(_workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table, qf);
            //    _bd.FilterResourceRecords(null, null, BatchData.FilterType.Clear);
            //}

            //{
            string _searchFilterText = string.Empty;
            string _searchFilterValue = string.Empty;

            _searchFilterText = drpSelection.Text;
            _searchFilterValue = txtSearch.Text;

            if (string.IsNullOrEmpty(_searchFilterText) && string.IsNullOrEmpty(_searchFilterValue))
            {
                //qf.WhereClause = string.Empty;
                MessageBox.Show("Please select a column and value to filter by", "Filter Error", MessageBoxButtons.OK);
            }

            if (!_searchFilterText.Equals(string.Empty))
            {
                //string[] arr = _searchFilterValue.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                //_searchFilterValue = String.Join("','", arr);
                //qf.WhereClause = _searchFilterText + " IN('" + _searchFilterValue + "')";
                _bd.FilterResourceRecords(_searchFilterText, _searchFilterValue, BatchData.FilterType.New);

                //_bd.BatchResourceRecords().Filter(_searchFilterText, _searchFilterValue);
            }
            //_VersionedResourceRecords = new TableWrapperFW(_workerBee.GetStandAloneTable("pgelibrary.geo.resources").Table, qf);
            //}

            batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();

            _resourcesFiltered = true;
            //qf.WhereClause = string.Empty;
        }

        private void btnAddFilter_Click(object sender, EventArgs e)
        {
            string _searchFilterText = string.Empty;
            string _searchFilterValue = string.Empty;

            _searchFilterText = drpSelection.Text;
            _searchFilterValue = txtSearch.Text;

            if (string.IsNullOrEmpty(_searchFilterText) && string.IsNullOrEmpty(_searchFilterValue))
            {
                //qf.WhereClause = string.Empty;
                MessageBox.Show("Please select a column and value to filter by", "Filter Error", MessageBoxButtons.OK);
            }

            if (!_searchFilterText.Equals(string.Empty))
            {
                //string[] arr = _searchFilterValue.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                //_searchFilterValue = String.Join("','", arr);

                if (_resourcesFiltered == false)
                {
                    //qf.WhereClause = _searchFilterText + " IN('" + _searchFilterValue + "')";
                    _bd.FilterResourceRecords(_searchFilterText, _searchFilterValue, BatchData.FilterType.New);
                }
                else
                {
                    //qf.WhereClause = qf.WhereClause + " OR " + _searchFilterText + " IN('" + _searchFilterValue + "')";
                    _bd.FilterResourceRecords(_searchFilterText, _searchFilterValue, BatchData.FilterType.Add);
                }
            }

            drpSelection.Text = string.Empty;
            txtSearch.Text = string.Empty;

            batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
            _resourcesFiltered = true;
        }





        private void resourceTabCntl_Click(object sender, EventArgs e)
        {
            GetResources();
        }





        public void GetResources()
        {
            _bd.SaveEdits(false);

            if (!String.IsNullOrEmpty(siteidTextBox.Text))
            {
                string primco = string.Empty;
                string primno = string.Empty;
                string trinno = string.Empty;
                string fsregion = string.Empty;
                string fsforest = string.Empty;
                string fsnump1 = string.Empty;
                string fsnump2 = string.Empty;
                string hrinumber = string.Empty;
                string othername = string.Empty;

                primco = txtprimco.Text == "" ? "null" : txtprimco.Text;
                primno = txtprimno.Text == "" ? "null" : txtprimno.Text;
                trinno = txttrinno.Text == "" ? "null" : txttrinno.Text;
                fsregion = txtfsregion.Text == "" ? "null" : txtfsregion.Text;
                fsforest = txtfsforest.Text == "" ? "null" : txtfsforest.Text;
                fsnump1 = txtfsnump1.Text == "" ? "null" : txtfsnump1.Text;
                fsnump2 = txtfsnump2.Text == "" ? "null" : txtfsnump2.Text;
                hrinumber = hrinumberTextBox.Text == "" ? "null" : "'" + hrinumberTextBox.Text + "'";
                othername = othernameTextBox.Text == "" ? "null" : "'" + othernameTextBox.Text + "'";

                DataSet dsSites = new DataSet();
                DataTable dtSites = new DataTable();
                NpgsqlConnection conn = new NpgsqlConnection(PGELibraryAddin.Properties.Settings.Default.pgsqlConnectionString);
                
                conn.Open();
                string sql = "SELECT * FROM resource_match_rank(" + primco + "," + primno + "," + trinno + "," + fsregion + "," + fsforest + "," + fsnump1 + "," + fsnump2 + "," + othername + "," + hrinumber + "," + siteidTextBox.Text + ")";
                NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
                
                dsSites.Reset();
                if (!chkSitesMatchingDisabled.Checked)
                {
                    da.Fill(dsSites);
                    dtSites.Reset();
                    dtSites = dsSites.Tables[0];
                    dataGridView2.DataSource = dtSites;
                } else
                {
                    dataGridView2.DataSource = null;
                }
                

                //at change vesion we should be setting the proper SQL version so this should work
                DataSet dsReports = new DataSet();
                DataTable dtReports = new DataTable();

                sql = "Select geo.reports_evw.reportid, geo.reports_evw.label, geo.reports_evw.reportnumber, geo.reports_evw.reporttitle, geo.reports_evw.year, geo.reports_evw.author " +
                     "FROM geo.reports_evw,geo.resourcereportevents_evw WHERE  " +
                     "geo.resourcereportevents_evw.reportid=geo.reports_evw.reportid   AND resourcereportevents_evw.resourceid = " + siteidTextBox.Text;
                string _sqlString1 = "SELECT sde.sde_set_current_version('" + _bd.VersionName().Replace(_bd.WorkspaceUser()+".",string.Empty) + "');";

                NpgsqlCommand cmd1 = new NpgsqlCommand(_sqlString1, conn);
                NpgsqlDataAdapter daR = new NpgsqlDataAdapter(sql, conn);
                cmd1.ExecuteNonQuery();
                dsReports.Reset();
                daR.Fill(dsReports);
                dtReports.Reset();
                dtReports = dsReports.Tables[0];
                dgvResourceReport.DataSource = dtReports;

                DataSet dsXref = new DataSet();
                DataTable dtXref = new DataTable();

                sql = "SELECT geo.lookupresourcexrefs.xrefdescription, geo.resources_evw.primarylabel, geo.resources_evw.trinomiallabel, geo.resources_evw.fslabel, geo.resources_evw.hrinumber, geo.resources_evw.othername, geo.xrefsresource_evw.resourceid, geo.xrefsresource_evw.xrefcode, geo.xrefsresource_evw.xrefid " +
                "FROM geo.xrefsresource_evw, geo.resources_evw, geo.lookupresourcexrefs " +
                "WHERE geo.xrefsresource_evw.xrefid = geo.resources_evw.resourceid AND geo.xrefsresource_evw.xrefcode = geo.lookupresourcexrefs.xrefcode AND geo.xrefsresource_evw.resourceid = " + siteidTextBox.Text;
                NpgsqlDataAdapter daX = new NpgsqlDataAdapter(sql, conn);

                daX.Fill(dsXref);
                dtXref.Reset();
                dtXref = dsXref.Tables[0];
                dgvResourceXref.DataSource = dtXref;
                conn.Close();
            }



        }
        //private DataTable TableSubset(TableWrapperFW wrappedTable, string idfield, string idvalue)
        //{
        //    IEnumerator 
        //    e = wrappedTable.GetEnumerator();



        //}
        private void bindingNavigatorPositionItem_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void btnReportAddFilter_Click(object sender, EventArgs e)
        {
            string _searchReportFilterText = string.Empty;
            string _searchReportFilterValue = string.Empty;

            _searchReportFilterText = drpReportSelect.Text;
            _searchReportFilterValue = txtReportSelectValue.Text;

            if (string.IsNullOrEmpty(_searchReportFilterText) && string.IsNullOrEmpty(_searchReportFilterValue))
            {
                MessageBox.Show("Please enter a column and a value to search", "Report filter error", MessageBoxButtons.OK);
            }

            if (!_searchReportFilterText.Equals(string.Empty))
            {
                //string[] arr = _searchReportFilterValue.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                //_searchReportFilterValue = String.Join("','", arr);

                if (_reportsFiltered == false)
                {
                    _bd.FilterReportRecords(_searchReportFilterText, _searchReportFilterValue, BatchData.FilterType.New);
                }
                else
                {
                    _bd.FilterReportRecords(_searchReportFilterText, _searchReportFilterValue, BatchData.FilterType.Add);
                }
            }

            drpReportSelect.Text = string.Empty;
            txtReportSelectValue.Text = string.Empty;
            batchReportBindingSource.DataSource = _bd.BatchReportRecords();
            _reportsFiltered = true;
        }

        private void btnReportFilter_Click(object sender, EventArgs e)
        {
            FilterReports();
           // batchReportBindingSource.Filter = "reportid in (";
        }

        private void btnReportClearFilter_Click_1(object sender, EventArgs e)
        {
            //FWTableWrapper _VersionedReportRecords = new FWTableWrapper(_workerBee.GetStandAloneTable("pgelibrary.geo.reports").Table, null);
            batchReportBindingSource.DataSource = _bd.BatchReportRecords();
            drpReportSelect.Text = "";
            txtReportSelectValue.Text = string.Empty;
        }
        private void FilterReports()
        {

            string _searchReportFilterText = string.Empty;
            string _searchReportFilterValue = string.Empty;

            _searchReportFilterText = drpReportSelect.Text;
            _searchReportFilterValue = txtReportSelectValue.Text;

            if (string.IsNullOrEmpty(_searchReportFilterText) && string.IsNullOrEmpty(_searchReportFilterValue))
            {
                MessageBox.Show("Please enter a column and a value to search", "Report filter error", MessageBoxButtons.OK);
            }

            if (!_searchReportFilterText.Equals(string.Empty))
            {


                if (_reportsFiltered == false)
                {
                    _bd.FilterReportRecords(_searchReportFilterText, _searchReportFilterValue, BatchData.FilterType.New);
                }
                else
                {
                    _bd.FilterReportRecords(_searchReportFilterText, _searchReportFilterValue, BatchData.FilterType.Add);
                }
            }

            drpReportSelect.Text = string.Empty;
            txtReportSelectValue.Text = string.Empty;
            batchReportBindingSource.DataSource = _bd.BatchReportRecords();
            _reportsFiltered = true; 
        }
        public void GetReports()
        {
            _bd.SaveEdits(false);

            if ((this.reportList.SelectedTab == this.tabReportDetail && !string.IsNullOrEmpty(txtReportID.Text)) ||
                (this.reportList.SelectedTab == this.tabReportMPE && !string.IsNullOrEmpty(txtReportIDMPE.Text)))
            {
                string sql;
                NpgsqlConnection conn;
                  conn = new NpgsqlConnection(PGELibraryAddin.Properties.Settings.Default.pgsqlConnectionString);
                if (this.reportList.SelectedTab == this.tabReportDetail)
                {
                    string repnumber = string.Empty;
                    string title = string.Empty;
                    string year = string.Empty;
                    string author = string.Empty;

                    repnumber = txtReportNumber.Text == "" ? "null" : txtReportNumber.Text;
                    title = txtReportTitle.Text == "" ? "null" : txtReportTitle.Text;
                    year = txtYear.Text == "" ? "null" : txtYear.Text;
                    author = txtAuthor.Text == "" ? "null" : txtAuthor.Text;

                    DataSet dsReports = new DataSet();
                    DataTable dtReports = new DataTable();
                   conn.Open();
                    string sqlReports = "SELECT * FROM report_match_rank('" + repnumber + "','" + title.Replace("'", "''") + "','" + year + "','" + author + "'," + txtReportID.Text + ")";
                    NpgsqlDataAdapter daReports = new NpgsqlDataAdapter(sqlReports, conn);
                    dsReports.Reset();
                    if (!chkReportMatchDisabled.Checked) {
                        daReports.Fill(dsReports);
                        dtReports.Reset();
                        dtReports = dsReports.Tables[0];
                        dataGridView3.DataSource = dtReports;
                    } else
                    {
                        dataGridView3.DataSource = null;
                    }

                    DataSet dsReportXref = new DataSet();
                    DataTable dtReportXref = new DataTable();
                     sql = "SELECT geo.lookupresourcexrefs.xrefdescription, geo.reports_evw.label, geo.reports_evw.reportnumber, geo.reports_evw.reporttitle, geo.reports_evw.year, geo.reports_evw.author " +
                        "FROM geo.xrefreport_evw, geo.reports_evw, geo.lookupresourcexrefs " +
                        "WHERE geo.xrefreport_evw.xrefid = geo.reports_evw.reportid " +
                        "AND geo.xrefreport_evw.xrefcode = geo.lookupresourcexrefs.xrefcode " +
                        "AND geo.xrefreport_evw.reporteventid = " + txtReportID.Text;
                    string _sqlString1 = "SELECT sde.sde_set_current_version('" + _bd.VersionName().Replace(_bd.WorkspaceUser()+".",string.Empty) + "');";

                    NpgsqlCommand cmd1 = new NpgsqlCommand(_sqlString1, conn);
                    NpgsqlDataAdapter daR = new NpgsqlDataAdapter(sql, conn);
                    cmd1.ExecuteNonQuery();
                    dsReportXref.Reset();
                    daR.Fill(dsReportXref);
                    dtReportXref.Reset();
                    dtReportXref = dsReportXref.Tables[0];
                    dgvReportXref.DataSource = dtReportXref;

                    DataSet dsResources = new DataSet();
                    DataTable dtResources = new DataTable();

                    sql = "SELECT geo.resources_evw.primarylabel, geo.resources_evw.trinomiallabel, geo.resources_evw.fslabel, geo.resources_evw.hrinumber, geo.resources_evw.othername, geo.resourcereportevents_evw.resourceid FROM geo.resourcereportevents_evw, geo.resources_evw WHERE geo.resourcereportevents_evw.resourceid = geo.resources_evw.resourceid AND geo.resourcereportevents_evw.reportid = " + txtReportID.Text;
                    NpgsqlDataAdapter daResources = new NpgsqlDataAdapter(sql, conn);

                    daResources.Fill(dsResources);
                    dtResources.Reset();
                    dtResources = dsResources.Tables[0];
                    dgvReportResource.DataSource = dtResources;
                    conn.Close();
                } else if (reportList.SelectedTab == tabReportMPE)
                {
                    mpereportBindingSource.DataSource = _bd.BatchMPEReportRecords("reportid = " + txtReportIDMPE.Text);

                    if (mpereportBindingSource.Count == 0)
                    {
                        enableMPEControls(false);
                        dgvMPEResource.DataSource = null;
                        dgvPersonnel.DataSource = null;
                    } else
                    {
                        enableMPEControls(true);
                        FWTableWrapper mpeResWrappped = _bd.BatchMPEResourceRecords("reportid = " + txtReportIDMPE.Text);
                        dgvMPEResource.DataSource = mpeResWrappped;
                        SetFieldVisibility( dgvMPEResource,SettingsGroup.MPERes);
                        DataSet dsRPM = new DataSet();
                        DataTable dtRPM = new DataTable();
                        if (string.IsNullOrEmpty(cboActivity.SelectedText))
                        {
                            sql = "SELECT id, rpmlabel, activity FROM geo.lookupmperpm;";
                        } else
                        {
                            sql = "SELECT id, rpmlabel FROM geo.lookupmperpm WHERE activity = '" + cboActivity.SelectedText + "';";
                        }
                        conn.Open();
                        NpgsqlDataAdapter daRPM = new NpgsqlDataAdapter(sql, conn);
                        daRPM.Fill(dsRPM);
                        dtRPM.Reset();
                        dtRPM = dsRPM.Tables[0];
                        conn.Close();
                        //TODO
                        //sloppy way to add checkbox columns
                        //should have generic code for generating column and adding to dgv
                        DataGridViewComboBoxColumn c = new DataGridViewComboBoxColumn();
                        c.DataSource = dtRPM;
                        c.Name = "rpm";
                        c.DataPropertyName = "rpm";
                        c.DisplayMember = "rpmlabel";
                        c.HeaderText = "rpm";
                        c.DisplayIndex = 1;
                        DataGridViewCheckBoxColumn c2 = new DataGridViewCheckBoxColumn();
                        c2.DataPropertyName = "inadvertent";
                        c2.Name = "inadvertent";
                        c2.HeaderText = "inadvertent";
                        c2.DisplayIndex = 2;
                        DataGridViewCheckBoxColumn c3 = new DataGridViewCheckBoxColumn();
                        c3.DataPropertyName = "monitored";
                        c3.Name = "monitored";
                        c3.HeaderText = "monitored";
                        c3.DisplayIndex = 3;
                        DataGridViewCheckBoxColumn c4 = new DataGridViewCheckBoxColumn();
                        c4.DataPropertyName = "f11a";
                        c4.Name = "f11a";
                        c4.HeaderText = "1.1(a)";
                        c4.DisplayIndex = 4;
                        DataGridViewCheckBoxColumn c5 = new DataGridViewCheckBoxColumn();
                        c5.DataPropertyName = "f12";
                        c5.Name = "f12";
                        c5.HeaderText = "1.2";
                        c5.DisplayIndex = 5;
                        DataGridViewCheckBoxColumn c6 = new DataGridViewCheckBoxColumn();
                        c6.DataPropertyName = "f131";
                        c6.Name = "f131";
                        c6.HeaderText = "1.3(1)";
                        c6.DisplayIndex = 6;
                        DataGridViewCheckBoxColumn c7 = new DataGridViewCheckBoxColumn();
                        c7.DataPropertyName = "f14";
                        c7.Name = "f14";
                        c7.HeaderText = "1.4";
                        c7.DisplayIndex = 7;
                        DataGridViewCheckBoxColumn c8 = new DataGridViewCheckBoxColumn();
                        c8.DataPropertyName = "f15";
                        c8.Name = "f15";
                        c8.HeaderText = "1.5";
                        c8.DisplayIndex = 8;
                        DataGridViewCheckBoxColumn c9 = new DataGridViewCheckBoxColumn();
                        c9.DataPropertyName = "f21a";
                        c9.Name = "f21a";
                        c9.HeaderText = "2.1(a)";
                        c9.DisplayIndex = 9;
                        DataGridViewCheckBoxColumn c10 = new DataGridViewCheckBoxColumn();
                        c10.DataPropertyName = "f21b";
                        c10.Name = "f21b";
                        c10.HeaderText = "2.1(b)";
                        c10.DisplayIndex = 10;
                        DataGridViewCheckBoxColumn c11 = new DataGridViewCheckBoxColumn();
                        c11.DataPropertyName = "f21c";
                        c11.Name = "f21c";
                        c11.HeaderText = "2.1(c)";
                        c11.DisplayIndex = 11;
                        DataGridViewCheckBoxColumn c12 = new DataGridViewCheckBoxColumn();
                        c12.DataPropertyName = "f21d";
                        c12.Name = "f21d";
                        c12.HeaderText = "2.1(d)";
                        c12.DisplayIndex = 12;
                        DataGridViewCheckBoxColumn c13 = new DataGridViewCheckBoxColumn();
                        c13.DataPropertyName = "f22a1";
                        c13.Name = "f22a1";
                        c13.HeaderText = "2.2(a)(1)";
                        c13.DisplayIndex = 13;
                        DataGridViewCheckBoxColumn c14 = new DataGridViewCheckBoxColumn();
                        c14.DataPropertyName = "f22a3";
                        c14.Name = "f22a3";
                        c14.HeaderText = "2.2(a)(3)";
                        c14.DisplayIndex = 14;
                        DataGridViewCheckBoxColumn c15 = new DataGridViewCheckBoxColumn();
                        c15.DataPropertyName = "f22a4";
                        c15.Name = "f22a4";
                        c15.HeaderText = "2.2(a)(4)";
                        c15.DisplayIndex = 15;
                        DataGridViewCheckBoxColumn c16 = new DataGridViewCheckBoxColumn();
                        c16.DataPropertyName = "f22a5";
                        c16.Name = "f22a5";
                        c16.HeaderText = "2.2(a)(5)";
                        c16.DisplayIndex = 16;
                        DataGridViewCheckBoxColumn c17 = new DataGridViewCheckBoxColumn();
                        c17.DataPropertyName = "f22b1c";
                        c17.Name = "f22b1c";
                        c17.HeaderText = "2.2(b)(1)(c)";
                        c17.DisplayIndex = 17;
                        DataGridViewCheckBoxColumn c18 = new DataGridViewCheckBoxColumn();
                        c18.DataPropertyName = "f22b1d";
                        c18.Name = "f22b1d";
                        c18.HeaderText = "2.2(b)(1)(d)";
                        c18.DisplayIndex = 18;
                        DataGridViewCheckBoxColumn c19 = new DataGridViewCheckBoxColumn();
                        c19.DataPropertyName = "f22b1g";
                        c19.Name = "f22b1g";
                        c19.HeaderText = "2.2(b)(1)(g)";
                        c19.DisplayIndex = 19;
                        DataGridViewCheckBoxColumn c20 = new DataGridViewCheckBoxColumn();
                        c20.DataPropertyName = "f22b1i";
                        c20.Name = "f22b1i";
                        c20.HeaderText = "2.2(b)(1)(i)";
                        c20.DisplayIndex = 20;
                        DataGridViewCheckBoxColumn c21 = new DataGridViewCheckBoxColumn();
                        c21.DataPropertyName = "f22b1j";
                        c21.Name = "f22b1j";
                        c21.HeaderText = "2.2(b)(1)(j)";
                        c21.DisplayIndex = 21;
                        DataGridViewComboBoxColumn c22 = new DataGridViewComboBoxColumn();
                        c22.DataPropertyName = "newupdate";
                        c22.Items.AddRange(new string[] { "New", "Update", "None" });
                        c22.Name = "newupdate";
                        c22.HeaderText = "New Update";
                        c22.DisplayIndex = 22;
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["rpm"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["inadvertent"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["monitored"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f11a"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f12"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f131"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f14"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f15"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f21a"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f21b"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f21c"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f21d"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22a1"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22a3"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22a4"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22a5"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22b1c"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22b1d"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22b1g"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22b1i"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["f22b1j"]);
                        dgvMPEResource.Columns.Remove(dgvMPEResource.Columns["newupdate"]);
                        dgvMPEResource.Columns.Add(c);
                        dgvMPEResource.Columns.Add(c2);
                        dgvMPEResource.Columns.Add(c3);
                        dgvMPEResource.Columns.Add(c4);
                        dgvMPEResource.Columns.Add(c5);
                        dgvMPEResource.Columns.Add(c6);
                        dgvMPEResource.Columns.Add(c7);
                        dgvMPEResource.Columns.Add(c8);
                        dgvMPEResource.Columns.Add(c9);
                        dgvMPEResource.Columns.Add(c10);
                        dgvMPEResource.Columns.Add(c11);
                        dgvMPEResource.Columns.Add(c12);
                        dgvMPEResource.Columns.Add(c13);
                        dgvMPEResource.Columns.Add(c14);
                        dgvMPEResource.Columns.Add(c15);
                        dgvMPEResource.Columns.Add(c16);
                        dgvMPEResource.Columns.Add(c17);
                        dgvMPEResource.Columns.Add(c18);
                        dgvMPEResource.Columns.Add(c19);
                        dgvMPEResource.Columns.Add(c20);
                        dgvMPEResource.Columns.Add(c21);
                        dgvMPEResource.Columns.Add(c22);

                        //dgvMPEResource.Columns["monitored"].DefaultCellStyle = 
                        //mpepersonnelBindingSource.DataSource = _bd.BatchMPEPersonnelRecords();
                        dgvPersonnel.DataSource = _bd.BatchMPEPersonnelRecords("reportid = " + txtReportIDMPE.Text);
                        DataGridViewCheckBoxColumn c23 = new DataGridViewCheckBoxColumn();
                        c23.DataPropertyName = "field";
                        c23.Name = "field";
                        c23.HeaderText = "field";
                        c23.DisplayIndex = 23;
                        SetFieldVisibility(dgvPersonnel, SettingsGroup.MPEPerson);
                        dgvPersonnel.Columns["person"].DisplayIndex = 0;
                        dgvPersonnel.Columns["company"].DisplayIndex = 1;
                        dgvPersonnel.Columns.Remove(dgvPersonnel.Columns["field"]);
                        dgvPersonnel.Columns.Add(c23);
                    }
                }
            }

        }
        private void customDGVColumn(DataGridView dgv,string newColumnName, columnTypes column)
        {


            switch (column)
            {
                case columnTypes.checkbox:
                    break;
                case columnTypes.combo:
                    break;
                case columnTypes.text:
                    break;

                default:
                    break;
            }

        }
        private void enableMPEControls(bool b)
        {
            this.cboClass.Enabled = b;
            this.cboActivity.Enabled = b;
            this.txtNotificationID.Enabled = b;
            this.surveyStart.Enabled = b;
            this.surveyEnd.Enabled = b;
            this.chkActDesktop.Enabled = b;
            this.chkActSurvey.Enabled = b;
            this.chkActMonitor.Enabled = b;
            this.chkActTest.Enabled = b;
            this.txtContraints.Enabled = b;
            this.notificationDate.Enabled = b;
            this.txtNumSites.Enabled = b;
            this.btnAddMPEResource.Enabled = b;
            this.btnAddMPEPerson.Enabled = b;
            this.cboForest.Enabled = b;
            this.txtIntensiveAcres.Enabled = b;
            this.txtNonintensiveAcres.Enabled = b;
            if(mpereportBindingSource.Count > 0)
            {
                this.btnDeleteMPERecord.Enabled = true;
                this.btnAddMPERecord.Enabled = false;
            } else
            {
                this.btnDeleteMPERecord.Enabled = false;
                this.btnAddMPERecord.Enabled = true;
            }
        }
        private void SaveWork()
        {
            _bd.SaveEdits(true);

            //            batchDataset1.mpereport
            //mpepersonnelTableAdapter.Update(batchDataset1.mpepersonnel);
            //mpereportTableAdapter1.Update(batchDataset1.mpereport);
            //mperesourceTableAdapter.Update(batchDataset1.mperesource);
            //reportsTableAdapter.Update(batchDataset1.reports);
            //resourcesTableAdapter.Update(batchDataset1.resources);
            //BatchDatasetTableAdapters.resourcesTableAdapter
        }

        private void reportList_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMoveNextItem1_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMoveLastItem1_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMovePreviousItem1_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMoveFirstItem1_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorPositionItem1_Click(object sender, EventArgs e)
        {
            GetReports();
        }




        private void button5_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(pdfTextBox.Text))
            {
                System.Diagnostics.Process.Start(pdfTextBox.Text);
            }
        }

        private void btnSelectSitePDF_Click(object sender, EventArgs e)
        {
            ofdPDF.Title = "Select a Resource PDF";
            ofdPDF.Filter = "PDF (*.pdf)";
            ofdPDF.RestoreDirectory = true;
            if (ofdPDF.ShowDialog() == DialogResult.OK)
            {
                pdfTextBox.Text = ofdPDF.FileName;
            }
        }

        private void btnOpenOrigPDf_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(textOrigPDF.Text))
            {
                System.Diagnostics.Process.Start(textOrigPDF.Text);
            }
        }

        private void btnOpenReportPDF_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(txtPDF.Text))
            {
                System.Diagnostics.Process.Start(txtPDF.Text);
            }
        }

        private void btnSelectReportPDF_Click(object sender, EventArgs e)
        {

        }

        private void btnOpenOrigReportPDF_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(txtOrigPath.Text))
            {
                System.Diagnostics.Process.Start(txtOrigPath.Text);
            }
        }

        private void btnReportAddXref_Click(object sender, EventArgs e)
        {
            LookupForm f = new LookupForm();
            f.SQLSource = "Select geo.reports_evw.reportid, geo.reports_evw.label, geo.reports_evw.reportnumber, " +
                "geo.reports_evw.reporttitle, geo.reports_evw.year, geo.reports_evw.author FROM geo.reports_evw " +
                "WHERE geo.reports_evw.label LIKE @p1 OR geo.reports_evw.reportnumber LIKE @p2  OR geo.reports_evw.reporttitle LIKE @p3  " +
                "OR geo.reports_evw.year LIKE @p4  OR geo.reports_evw.author LIKE @p5 ";
            f.FormPurpose = LookupForm.LookupType.ReportXRef;
            f.InstructionText = "Select a report.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(this.txtReportID.Text);
            f.RelationshipVisible = true;
            f.ShowDialog();
        }

        private void tnReportAddResource_Click(object sender, EventArgs e)
        {
            LookupForm f = new LookupForm();
            f.SQLSource = "SELECT geo.resources_evw.resourceid, geo.resources_evw.primarylabel, geo.resources_evw.trinomiallabel, geo.resources_evw.fslabel, geo.resources_evw.othername,geo.resources_evw.hrinumber " +
            "FROM geo.resources_evw WHERE geo.resources_evw.primarylabel LIKE @p1 OR " +
            "geo.resources_evw.trinomiallabel LIKE @p2 OR geo.resources_evw.fslabel LIKE @p3 OR " +
            "geo.resources_evw.othername LIKE @p4 OR geo.resources_evw.hrinumber LIKE @p5";
            f.FormPurpose = LookupForm.LookupType.ResourceForReport;
            f.InstructionText = "Select a related resource.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(this.txtReportID.Text);
            f.RelationshipVisible = false;
            f.ShowDialog();
            this.dgvResourceXref.Refresh();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LookupForm f = new LookupForm();
            f.SQLSource = "SELECT geo.resources_evw.resourceid, geo.resources_evw.primarylabel, geo.resources_evw.trinomiallabel, geo.resources_evw.fslabel, geo.resources_evw.othername,geo.resources_evw.hrinumber " +
            "FROM geo.resources_evw WHERE geo.resources_evw.primarylabel LIKE @p1 OR " +
            "geo.resources_evw.trinomiallabel LIKE @p2 OR geo.resources_evw.fslabel LIKE @p3 OR " +
            "geo.resources_evw.othername LIKE @p4 OR geo.resources_evw.hrinumber LIKE @p5";
            f.FormPurpose = LookupForm.LookupType.ResourceXRef;
            f.InstructionText = "Select a related resource.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(siteidTextBox.Text);
            f.RelationshipVisible = true;
            f.ShowDialog();
            this.dgvResourceXref.Refresh();
        }

        private void btnResourceAddReport_Click(object sender, EventArgs e)
        {
            LookupForm f = new LookupForm();
            f.SQLSource = "Select geo.reports_evw.reportid, geo.reports_evw.label, geo.reports_evw.reportnumber, " +
            "geo.reports_evw.reporttitle, geo.reports_evw.year, geo.reports_evw.author FROM geo.reports_evw " +
            "WHERE geo.reports_evw.label LIKE @p1 OR geo.reports_evw.reportnumber LIKE @p2  OR geo.reports_evw.reporttitle LIKE @p3  " +
            "OR geo.reports_evw.year LIKE @p4  OR geo.reports_evw.author LIKE @p5 ";
            f.FormPurpose = LookupForm.LookupType.ReportForResource;
            f.InstructionText = "Select a related report.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(this.siteidTextBox.Text);
            f.RelationshipVisible = false;
            f.ShowDialog();
            this.dgvResourceReport.Refresh();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    LookupForm f = new LookupForm();
        //    f.SQLSource = "Select geo.reports_evw.reportid, geo.reports_evw.label, geo.reports_evw.reportnumber, " +
        //        "geo.reports_evw.reporttitle, geo.reports_evw.year, geo.reports_evw.author FROM geo.reports_evw " +
        //        "WHERE geo.reports_evw.label LIKE @p1 OR geo.reports_evw.reportnumber LIKE @p2  OR geo.reports_evw.reporttitle LIKE @p3  " +
        //        "OR geo.reports_evw.year LIKE @p4  OR geo.reports_evw.author LIKE @p5 ";
        //    f.FormPurpose = LookupForm.LookupType.ReportForResource;
        //    f.InstructionText = "Select a report.";
        //    f.bd = _bd;
        //    f.RelationKey = Convert.ToInt32(siteidTextBox.Text);
        //    f.RelationshipVisible = false;
        //    f.ShowDialog();
        //    this.dgvResourceReport.Refresh();
        //}

        private void buttonCreateVersion_Click_1(object sender, EventArgs e)
        {
            //add some checks
            //these should highlight as you enter
            IList<string> bnames = (IList<string>)comboBatchNames.DataSource;
            if (textPGENumber.Text != null && textPGENumber.Text != "")
            {
                if (System.Convert.ToInt32(textPGENumber.Text) > 2147483647)
                {
                    MessageBox.Show("PG&E Number must be less than 2147483647", "Invalid PG&E number", MessageBoxButtons.OK);
                    return;
                }
            }
            if (textFWJobNumber.Text != null & textFWJobNumber.Text != "")
            {
                if (System.Convert.ToInt32(textFWJobNumber.Text) > 9999)
                {
                    MessageBox.Show("FW Number must be less than 9999", "Invalid PG&E number", MessageBoxButtons.OK);
                    return;
                }
            }

            if (bnames.Contains(_bd.WorkspaceUser() + "." + textNewBatchName.Text))
            {
                //if (bnames.Contains(textNewBatchName.Text))

                MessageBox.Show("Version name exists. Please try again.", "Invalid Batch Name", MessageBoxButtons.OK);
                return;
            }
            if (String.IsNullOrEmpty(txtBatchPath.Text))
            {
                if (MessageBox.Show("Proceed without a batch path?", "Path missing?", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }
            if (BatchNameExists(textNewBatchName.Text))
            {
                MessageBox.Show("This batch name exists in the batch table. Please choose another name.", "Batch Name Exists", MessageBoxButtons.OK);
                return;
            }
            if (textNewBatchName.Text.Length > 62)
            {
                MessageBox.Show("Please use a batch name less than 62 characters.","Long Batch Name",MessageBoxButtons.OK);
            }
            _bd.CreateVersion(textNewBatchName.Text, System.Convert.ToInt32(textFWJobNumber.Text), System.Convert.ToInt32(textPGENumber.Text), textComment.Text, txtBatchPath.Text);
            RefreshBatchData(false);
            //batchResourceBindingSource.DataSource = _bd.BatchResourceRecords();
            // batchReportBindingSource.DataSource = _bd.BatchReportRecords();
        }
        private Boolean BatchNameExists(string batchname)
        {
                //this will return for the default version. if the version hasn't been posted then this batch does not exist.
                IQueryFilter qf = new QueryFilterClass();
                qf.WhereClause = "batchname ='" + batchname + "'";
                IStandaloneTable batchStandAloneTble = _workerBee.GetStandAloneTable("pgelibrary.geo.batch");
                ITable tbl = batchStandAloneTble.Table;
                ICursor cur;
                cur = tbl.Search(qf, false);
                IRow r;
                r = cur.NextRow();
                if (r != null)
                {
                    return true;
                }
                return false; 
        }
        private Boolean LoadExistingBatch(string batchname)
        {
            int firstdot;
            firstdot = batchname.IndexOf(".")+1;
            batchname = batchname.Substring(firstdot, batchname.Length - firstdot);
            //this will return for the default version. if the version hasn't been posted then this batch does not exist.
            IQueryFilter qf = new QueryFilterClass();
            qf.WhereClause = "batchname ='" + batchname + "'";

            IStandaloneTable batchStandAloneTble = _workerBee.GetStandAloneTable("pgelibrary.geo.batch");
            ITable tbl = batchStandAloneTble.Table;

            ICursor cur;
            cur = tbl.Search(qf, false);
            IRow r;
            r = cur.NextRow();
            if (r != null)
            {
                textNewBatchName.Text = r.get_Value(r.Fields.FindField("batchname")) as string;
                txtBatchPath.Text = r.get_Value(r.Fields.FindField("batchpath")) as string;
                textFWJobNumber.Text = r.get_Value(r.Fields.FindField("fwjobnumber")) as string;
                textPGENumber.Text = r.get_Value(r.Fields.FindField("pgeordernumber")) as string;
                txtComment.Text = r.get_Value(r.Fields.FindField("comment")) as string;
            }
            return false;

        }
        private void button2_Click(object sender, EventArgs e)
        {
            _bd.ImportToBatch(BatchData.ImportType.ReportPDF, optReportTitle.Checked);
            RefreshBatchData(false);
            //batchReportBindingSource.ResetBindings(false);

            //batchReportBindingSource.DataSource = null;
            //batchReportBindingSource.DataSource = _bd.BatchReportRecords();
            //reportsDataGridView.DataSource = typeof(List<>);
            //reportsDataGridView.DataSource = batchReportBindingSource;
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            _bd.CompleteBatch();
            //_bd = null;
            //GC.Collect();

            //FeatureLayerList flList = new FeatureLayerList();
            //flList = _workerBee.GetLayers(null, null, null);
            //IFeatureLayer fl = flList.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.ResourcePolyName);
            //IWorkspace workspace;
            //workspace = ((IDataset)fl).Workspace;
            //IPropertySet2 propertySet;
            //propertySet = (IPropertySet2)workspace.ConnectionProperties;
            //object obj = propertySet.GetProperty("VERSION");

            //IWorkspace2 wksp = _workerBee.GetSettingsDefinedWorkspace("esriDataSourcesGDB.SdeWorkspaceFactory", PGELibraryAddin.Properties.Settings.Default.UserSDEConnectionFile, obj.ToString());
            //IVersionedWorkspace4 Vwksp = (IVersionedWorkspace4)wksp;
            //IVersion Vchild = (IVersion3)Vwksp;

            //Vchild.Delete();
        }

        private void btnOpenOrigReportPDF_Click_1(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(txtOrigPath.Text))
            {
                System.Diagnostics.Process.Start(txtOrigPath.Text);
            }
        }

        private void btnOpenReportPDF_Click_1(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(txtPDF.Text))
            {
                System.Diagnostics.Process.Start(txtPDF.Text);
            }
        }

        private void btnOpenSitePDF_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(pdfTextBox.Text))
            {
                System.Diagnostics.Process.Start(pdfTextBox.Text);
            }
        }

        private void btnOpenOrigPDf_Click_1(object sender, EventArgs e)
        {

            if (System.IO.File.Exists(this.textOrigPDF.Text))
            {
                System.Diagnostics.Process.Start(textOrigPDF.Text);
            }
        }

        private void btnSelectReportPDF_Click_1(object sender, EventArgs e)
        {
            ofdPDF.Title = "Select a Report PDF";
            ofdPDF.Filter = "PDF (*.pdf)|*.pdf|All files (*.*)|*.*";
            ofdPDF.RestoreDirectory = true;
            if (ofdPDF.ShowDialog() == DialogResult.OK)
            {
                txtPDF.Text = ofdPDF.FileName;
            }
        }

        private void btnSelectSitePDF_Click_1(object sender, EventArgs e)
        {
            ofdPDF.Title = "Select a Resource PDF";
            ofdPDF.Filter = "PDF (*.pdf)|*.pdf|All files (*.*)|*.*";
            ofdPDF.RestoreDirectory = true;
            if (ofdPDF.ShowDialog() == DialogResult.OK)
            {
                pdfTextBox.Text = ofdPDF.FileName;
            }
        }

        private void bindingNavigatorMoveNextItem_Click_1(object sender, EventArgs e)
        {
            GetResources();
        }

        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void bindingNavigatorMoveNextItem1_Click_1(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMoveFirstItem1_Click_1(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMovePreviousItem1_Click_1(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorMoveLastItem1_Click_1(object sender, EventArgs e)
        {
            GetReports();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            _bd.AppendResource(Convert.ToInt32(siteidTextBox.Text));
            RefreshBatchData(false);
        }

        private void bindingNavigatorAddNewItem1_Click(object sender, EventArgs e)
        {
            _bd.AppendReport(Convert.ToInt32(txtReportID.Text));
            RefreshBatchData(false);
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void textOrigPDF_Leave(object sender, EventArgs e)
        {
            _bd.ResourcePDFChanged(Convert.ToInt32(this.siteidTextBox.Text), textOrigPDF.Text);
        }

        private void txtOrigPath_Leave(object sender, EventArgs e)
        {
            _bd.ReportPDFChanged(Convert.ToInt32(this.txtReportID.Text), txtOrigPath.Text);
        }

        private void btnReportsUpdateMatch_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void btnSitesRefreshMatch_Click(object sender, EventArgs e)
        {
            GetResources();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Test");
        }

        private void buttonRefresh2_Click(object sender, EventArgs e)
        {
            RefreshBatchData(false);
        }

        private void buttonRefresh1_Click(object sender, EventArgs e)
        {
            RefreshBatchData(false);
        }

        private void lblLinearBlock_Click(object sender, EventArgs e)
        {

        }



        private void chkMissingSiteGIS_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMissingSiteGIS.Checked)
            {
                FeatureLayerList featureLayers = _workerBee.GetLayers(null, _bd.BatchWorkspace, null);
                IFeatureLayer fl;
                IFeatureLayerDefinition2 featDef;
                System.Collections.ArrayList idlist = new System.Collections.ArrayList();
                List<int> uniquelist;
                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "resourceid"));
                }

                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "resourceid"));
                }

                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "resourceid"));
                }
                string filterValues = string.Join(",", (string[])idlist.ToArray(Type.GetType("System.String")));
                _bd.FilterResourceRecords("resourceid", filterValues, BatchData.FilterType.GISAdd);
            }
            else
            {
                _bd.FilterResourceRecords(null, null, BatchData.FilterType.GISRemove);
            }
        }

        private void chkMissingReportGIS_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMissingReportGIS.Checked)
            {
                FeatureLayerList featureLayers = _workerBee.GetLayers(null, _bd.BatchWorkspace, null);
                IFeatureLayer fl;
                IFeatureLayerDefinition2 featDef;
                System.Collections.ArrayList idlist = new System.Collections.ArrayList();
                List<int> uniquelist;
                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoly);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "reportid"));
                }

                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportPoint);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "reportid"));
                }

                if (featureLayers.IndexOfName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear) != -1)
                {
                    fl = featureLayers.FeatureLayerByName(PGELibraryAddin.Properties.Settings.Default.BatchReportLinear);
                    idlist.AddRange(_workerBee.GetUniqueValuesForLayer(fl, "reportid"));
                }
                string filterValues = string.Join(",", (string[])idlist.ToArray(Type.GetType("System.String")));
                _bd.FilterReportRecords("reportid", filterValues, BatchData.FilterType.GISAdd);
            }
            else
            {
                _bd.FilterReportRecords(null, null, BatchData.FilterType.GISRemove);
            }
        }

        private void btnLoadGIS_Click(object sender, EventArgs e)
        {
            FeatureLayerList featureLayers = _workerBee.GetLayers(null, _bd.BatchWorkspace, null);
            IFeatureLayer fl;
            if (featureLayers.IndexOfName(cboGISLayer.SelectedValue.ToString()) != -1)
            {
                fl = featureLayers.FeatureLayerByName(cboGISLayer.SelectedValue.ToString());
                FWARGTableWrapper.FWTableWrapper GISTable = new FWTableWrapper((ITable)fl);
                gisTableBindingSource.DataSource = GISTable;
                dgvGISTable.DataSource = gisTableBindingSource;
                EditableSettings es = new EditableSettings();
                cboMatchType.Items.Clear();
                if (es.BatchReportLayerNames.Contains(cboGISLayer.SelectedValue.ToString()))
                {
                    //working with reports
                    cboMatchType.Items.AddRange(new string[] { "Report Label", "Report Number" });
                    dgvMatchTable.DataSource = reportsDataGridView.DataSource;
                    _GISKey = "reportid";
                }
                else
                {
                    cboMatchType.Items.AddRange(new string[] { "Primary", "Trinomial", "FS Number", "Other Name" });
                    dgvMatchTable.DataSource = sitesDataGridView.DataSource;
                    _GISKey = "resourceid";
                }
                cboMatchType.Enabled = true;
                dgvGISTable.AllowUserToOrderColumns = true;
                dgvGISTable.AllowUserToResizeColumns = true;
                dgvMatchTable.AllowUserToOrderColumns = true;
                dgvMatchTable.AllowUserToResizeColumns = true;
            }

        }

        private void cboMatchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //attempt to match numbers and populate ids
            string matchField = "";
            string GISField = "";

            
            System.Windows.Forms.BindingSource dbTable = null;
            //determine the appropriate field to search
            switch (cboMatchType.Text)
            {
                case "Report Label":
                    matchField = "label";
                    GISField = "comment";
                    _GISKey = "reportid";
                    dbTable = (BindingSource)reportsDataGridView.DataSource;
                    break;
                case "Report Number":
                    matchField = "reportnumber";
                    GISField = "comment";
                    _GISKey = "reportid";
                    dbTable = (BindingSource)reportsDataGridView.DataSource;
                    break;
                case "Primary":
                    matchField = "primarylabel";
                    GISField = "resourcelabel";
                    _GISKey = "resourceid";
                    dbTable = (BindingSource)sitesDataGridView.DataSource;
                    break;
                case "Trinomial":
                    matchField = "trinomiallabel";
                    GISField = "resourcelabel";
                    _GISKey = "resourceid";
                    dbTable = (BindingSource)sitesDataGridView.DataSource;
                    break;
                case "FS Number":
                    matchField = "fslabel";
                    GISField = "resourcelabel";
                    _GISKey = "resourceid";
                    dbTable = (BindingSource)sitesDataGridView.DataSource;
                    break;
                case "Other Name":
                    matchField = "othername";
                    GISField = "resourcelabel";
                    _GISKey = "resourceid";
                    dbTable = (BindingSource)sitesDataGridView.DataSource;
                    break;
                default:
                    break;

            }
            //loop through BatchResource/BatchReport tables and see if we can find an exact match

            string searchValue;
            string searchKey;
            int i;
            if (!(dbTable is null))
            {
                BindingSource bindingSource = (BindingSource)dgvGISTable.DataSource;

                FWTableWrapper GISTable = (FWTableWrapper)bindingSource.DataSource;
                 //populate the id field
                foreach (IRow r in GISTable)
                {
                    searchKey = r.Value[r.Fields.FindField(_GISKey)].ToString();
                    if (string.IsNullOrEmpty(searchKey)){
                        searchValue = r.Value[r.Fields.FindField(GISField)].ToString();
                        i = dbTable.Find(matchField, searchValue);
                        if (i >= 0)
                        {
                            DataRowView drv = (DataRowView)dbTable.List[i];
                            r.set_Value(r.Fields.FindField(_GISKey), drv[_GISKey]);
                        }
                    }
                }
 
            }
           
        
        }

        private void chkReportMatchDisabled_CheckedChanged(object sender, EventArgs e)
        {
            if (chkReportMatchDisabled.Checked)
            {
                chkReportMatchDisabled.BackColor = System.Drawing.Color.AliceBlue;
                chkReportMatchDisabled.Text = "Matching Disabled";
            } else
            {
                chkReportMatchDisabled.BackColor = System.Drawing.Color.WhiteSmoke;
                chkReportMatchDisabled.Text = "Matching Enabled";
            }
        }

        private void chkSitesMatchingDisabled_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSitesMatchingDisabled.Checked)
            {
                chkSitesMatchingDisabled.BackColor = System.Drawing.Color.AliceBlue;
                chkSitesMatchingDisabled.Text = "Matching Disabled";
            }
            else
            {
                chkSitesMatchingDisabled.BackColor = System.Drawing.Color.WhiteSmoke;
                chkSitesMatchingDisabled.Text = "Matching Enabled";
            }
        }



        private void dgvGISTable_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = dgvGISTable.PointToClient(new Point(e.X, e.Y));

            // If the drag operation was a copy then add the row to the other control.
            if (e.Effect == DragDropEffects.Copy)
            {
                //get's an int
                int cellvalue = (int)e.Data.GetData(e.Data.GetFormats()[0]);
                var hittest = dgvGISTable.HitTest(clientPoint.X, clientPoint.Y);
                if (hittest.ColumnIndex != -1
                    && hittest.RowIndex != -1)
                    dgvGISTable[dgvGISTable.Columns[_GISKey].Index, hittest.RowIndex].Value = cellvalue;

            }
        }
        // https://stackoverflow.com/questions/21131157/drag-and-drop-cell-from-datagridview-to-another
        private void dgvGISTable_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvGISTable_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private Rectangle dragBoxFromMouseDown;
        private object valueFromMouseDown;
        private void dgvMatchTable_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            var hittestInfo = dgvMatchTable.HitTest(e.X, e.Y);

            if (hittestInfo.RowIndex != -1 && hittestInfo.ColumnIndex != -1)
            {
                valueFromMouseDown = dgvMatchTable.Rows[hittestInfo.RowIndex].Cells[dgvMatchTable.Columns[_GISKey].Index].Value;
                if (valueFromMouseDown != null)
                {
                    // Remember the point where the mouse down occurred. 
                    // The DragSize indicates the size that the mouse can move 
                    // before a drag event should be started.                
                    Size dragSize = SystemInformation.DragSize;

                    // Create a rectangle using the DragSize, with the mouse position being
                    // at the center of the rectangle.
                    dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                }
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvMatchTable_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgvMatchTable.DoDragDrop(valueFromMouseDown, DragDropEffects.Copy);
                }
            }
        }

        private void cboGISLayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnLoadGIS.Enabled = true;
        }



        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            GetReports();
        }

        //private void SetFieldVisibility(System.Windows.Forms.DataGridView dg, System.Data.DataTable dgCols, StringCollection SettingsColumnOrder, StringCollection SettingsColumnVis)
        private void SetFieldVisibility(System.Windows.Forms.DataGridView dg, SettingsGroup settingsGroup)
        {
            int i;
            StringCollection SettingsColumnOrder;
            StringCollection SettingsColumnVis;
            StringCollection SettingsColumnHeader;

            if (settingsGroup == SettingsGroup.MPERes)
            {
                SettingsColumnOrder = GetSettingsCollection(dg, SettingsType.MPEResIndex);
                SettingsColumnVis = GetSettingsCollection(dg, SettingsType.MPEResVisiblity);
                SettingsColumnHeader = GetSettingsCollection(dg, SettingsType.MPEResHeader);
            } else //for use when we have more types
            {
                SettingsColumnOrder = GetSettingsCollection(dg, SettingsType.MPEPersonIndex);
                SettingsColumnVis = GetSettingsCollection(dg, SettingsType.MPEPersonVisiblity);
                SettingsColumnHeader = GetSettingsCollection(dg, SettingsType.MPEPersonsHeader);
            }

           
            for (i = 0; i <= SettingsColumnOrder.Count - 1; i++)
            {
                //DataRow r;
                //r = dgCols.NewRow();
                //r[0] = dg.Columns[i].HeaderText.Replace("pgelibrary.geo.mperesource.", "").Replace("pgelibrary.geo.resources.", "");
                //r[1] = System.Convert.ToBoolean(SettingsColumnVis[i]);
                //r[2] = dg.Columns[i].Name;
                //dgCols.Rows.Add(r);
                dg.Columns[i].DisplayIndex = System.Convert.ToInt32(SettingsColumnOrder[i]);
                dg.Columns[i].Visible = System.Convert.ToBoolean(SettingsColumnVis[i]);
                dg.Columns[i].HeaderText = SettingsColumnHeader[i];
                if (dg.Columns[i].HeaderText == "inadvertent" && dg.Columns[i].Equals(typeof(DataGridViewTextBoxColumn)))
                {
                    dg.Columns.Remove(dg.Columns[i]);
                }
            }
            if (settingsGroup == SettingsGroup.MPERes)
            {
                SetSettingsCollection(SettingsColumnHeader, SettingsType.MPEResHeader);
                SetSettingsCollection(SettingsColumnOrder, SettingsType.MPEResIndex);
                SetSettingsCollection(SettingsColumnVis, SettingsType.MPEResVisiblity);
            } else
            {
                SetSettingsCollection(SettingsColumnHeader, SettingsType.MPEPersonsHeader);
                SetSettingsCollection(SettingsColumnOrder, SettingsType.MPEPersonIndex);
                SetSettingsCollection(SettingsColumnVis, SettingsType.MPEPersonVisiblity);
            }
        }
        private void SetSettingsCollection(StringCollection Settings, SettingsType SettingType)
        {
            if (SettingType == SettingsType.MPEResHeader )
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnHeader = Settings;
            }
            else if (SettingType == SettingsType.MPEResIndex )
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnOrder = Settings;
            } else if(SettingType == SettingsType.MPEResVisiblity)
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceVisibleFields = Settings;
            }
            else if (SettingType == SettingsType.MPEPersonVisiblity) { 
                PGELibraryAddin.Properties.Settings.Default.MPEPersonnelVisibleFields = Settings;
            }
            else if (SettingType == SettingsType.MPEPersonIndex)
            {
                PGELibraryAddin.Properties.Settings.Default.MPEPersonnelOrder = Settings;
            }
            else if (SettingType == SettingsType.MPEPersonsHeader)
            {
                PGELibraryAddin.Properties.Settings.Default.MPEPersonnelHeader = Settings;
            }
        }
        private StringCollection GetSettingsCollection(System.Windows.Forms.DataGridView dg, SettingsType SettingType)
        {
            StringCollection OutSettings = new StringCollection();
            string v = "True";
            int i = 0;
            string headertext = "";

            if (SettingType == SettingsType.MPEResHeader && PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnHeader != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnHeader;
            }
            else if (SettingType == SettingsType.MPEResIndex && PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnOrder != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnOrder;
            }

            else if (SettingType == SettingsType.MPEResVisiblity && PGELibraryAddin.Properties.Settings.Default.MPEResourceVisibleFields != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceVisibleFields;
            }
            if (SettingType == SettingsType.MPEPersonsHeader && PGELibraryAddin.Properties.Settings.Default.MPEPersonnelHeader != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEPersonnelHeader;
            }
            else if (SettingType == SettingsType.MPEPersonIndex && PGELibraryAddin.Properties.Settings.Default.MPEPersonnelOrder != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEPersonnelOrder;
            }

            else if (SettingType == SettingsType.MPEPersonVisiblity && PGELibraryAddin.Properties.Settings.Default.MPEPersonnelVisibleFields != null)
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEPersonnelVisibleFields;
            }
            foreach (DataGridViewColumn c in dg.Columns)
            {
                if (SettingType == SettingsType.MPEResIndex || SettingType == SettingsType.MPEPersonIndex)
                {
                    // for the index we'll go with the order they are passed in
                    OutSettings.Add(i.ToString());
                    i += 1;
                } else if (SettingType == SettingsType.MPEResHeader || SettingType == SettingsType.MPEPersonsHeader)
                {
                    if (c.Name.Contains("pgelibrary."))
                    {
                        headertext = c.Name.Replace("pgelibrary.", "");
                    } else
                    {
                        headertext = c.Name;
                    }
                    if (headertext.Contains("geo."))
                    {
                        headertext = headertext.Replace("geo.", "");
                    }
                    if (headertext.Contains("mperesource."))
                    {
                        headertext = headertext.Replace("mperesource.", "");
                    }
                    if (headertext.Contains("resources."))
                    {
                        headertext = headertext.Replace("resources.", "");
                    }
                    if (headertext.Contains("mpereportpersonnel.") )
                    {
                        headertext = headertext.Replace("mpereportpersonnel.", "");
                    }
                    if (headertext.Contains("mpepersonnel.") )
                    {
                        headertext = headertext.Replace("mpepersonnel.", "");
                    }
                    OutSettings.Add(headertext);
                }
                else
                {
                    // these are a few default settings
                    System.Globalization.CompareInfo myComp = System.Globalization.CultureInfo.InvariantCulture.CompareInfo;
                    if (myComp.IndexOf(c.Name, "SHAPE", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "OBJECTID", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "primco", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "primno", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "trinno2", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "trinh", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "trinno", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "fsregion", System.Globalization.CompareOptions.IgnoreCase) > -1 || 
                        myComp.IndexOf(c.Name, "fsforest", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "fsnump1", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "fsnump2", System.Globalization.CompareOptions.IgnoreCase) > -1||
                        myComp.IndexOf(c.Name, "pdf", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "res_area", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "land_owner", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "comments", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "preelig", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "histelig", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "sitedatastatus", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "origfilepath", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "origsiteid", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "created_user", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "created_date", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "last_edited_user", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "last_edited_date", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "age", System.Globalization.CompareOptions.IgnoreCase) > -1 ||
                        myComp.IndexOf(c.Name, "description", System.Globalization.CompareOptions.IgnoreCase) > -1)
                        v = "False";
                    else
                        v = "True";
                    OutSettings.Add(v);
                }
            }

            return OutSettings;
        }

        private void btnAddMPERPerson_Click(object sender, EventArgs e)
        {
            //get
            //insert into mperesportpersonnel reportid personid
            if (string.IsNullOrEmpty(txtReportIDMPE.Text))
            {
                MessageBox.Show("No ReportID identified");
                return;
            }
        
            LookupForm f = new LookupForm();
            f.SQLSource = "Select geo.viewpersonnel.person, geo.viewpersonnel.company, geo.viewpersonnel.firmlookup, geo.viewpersonnel.status, geo.viewpersonnel.objectid " +
                 "FROM geo.viewpersonnel WHERE geo.viewpersonnel.person ILIKE @p1 OR geo.viewpersonnel.company ILIKE @p2  OR geo.viewpersonnel.firmlookup ILIKE @p3  " +
                "OR geo.viewpersonnel.status ILIKE @p4  ";
            f.FormPurpose = LookupForm.LookupType.PersonnelForMPE;
            f.InstructionText = "Select a person.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(this.txtReportIDMPE.Text);
           
            f.RelationshipVisible = false;
            f.ShowDialog();

            GetReports();
            this.dgvPersonnel.Refresh();
        }

        private void dgvMPEResource_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FormMPEResourceDetail f = new FormMPEResourceDetail();
            f.reportid = Convert.ToInt32(this.txtReportIDMPE.Text);
            f.resourceid = Convert.ToInt32(dgvMPEResource[dgvMPEResource.Columns["resourceid"].Index, e.RowIndex].Value);
            f.batchData = _bd;
            f.ShowDialog();
            GetReports();

            this.dgvMPEResource.Refresh();
        }

        private void btnAddMPEResource_Click(object sender, EventArgs e)
        {
            LookupForm f = new LookupForm();
            f.SQLSource = "SELECT geo.resources_evw.resourceid, geo.resources_evw.primarylabel, geo.resources_evw.trinomiallabel, geo.resources_evw.fslabel, geo.resources_evw.othername,geo.resources_evw.hrinumber " +
            "FROM geo.resources_evw WHERE geo.resources_evw.primarylabel LIKE @p1 OR " +
            "geo.resources_evw.trinomiallabel LIKE @p2 OR geo.resources_evw.fslabel LIKE @p3 OR " +
            "geo.resources_evw.othername LIKE @p4 OR geo.resources_evw.hrinumber LIKE @p5";
            f.FormPurpose = LookupForm.LookupType.ResourceForMPE;
            f.InstructionText = "Select a resource.";
            f.bd = _bd;
            f.RelationKey = Convert.ToInt32(this.txtReportIDMPE.Text);
            f.RelationshipVisible = false;
            f.ShowDialog();

            //open resource detail with Resourceid to add details
            GetReports();
            
        }



        private void bindingNavigatorDeleteItem1_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void btnAddMPERecord_Click(object sender, EventArgs e)
        {
            _bd.AppendMPEReport(Convert.ToInt32(this.txtReportIDMPE.Text));
            GetReports();
        }

        private void btnDeleteMPERecord_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete this and related records?", "Delete?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _bd.DeleteMPEReport(Convert.ToInt32(this.txtReportIDMPE.Text));
                GetReports();
            }
        }
    }
}