﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ESRI.ArcGIS.Geodatabase;
//using ArcDataBinding;
//using System.IO;
//using System.ComponentModel;
//using System.Windows.Forms;


//namespace PGELibraryAddin
//{
//    public class FWTableWrapper : TableWrapper, IBindingListView
//    {
//        private ITable wrappedTable;
//        // Private wrappedTableAll As IList(Of IRow)
//        private ITableSort wrappedTableSort;
//        private bool _isSorted = false;
//        private bool _isFiltered = false;
//        private ListSortDirection _sortDirection;
//        private PropertyDescriptor _sortProperty;
//        private string _Wildcard;
//        private string _DelimiterPre;
//        private string _DelimiterPost;
//        private string _filterValue = "";
//        private int _lastFoundIndx;

//        public FWTableWrapper(ITable tableToWrap) : base(tableToWrap)
//        {
//            wrappedTable = tableToWrap;

//            GetWorkspaceWildcard();
//            _lastFoundIndx = -1;
//        }
//        public void GetWorkspaceWildcard()
//        {
//            IWorkspace worksp;
//            worksp = ((IDataset)wrappedTable).Workspace;
//            ISQLSyntax sqlSyntx;
//            sqlSyntx = (ISQLSyntax)worksp;
//            _Wildcard = sqlSyntx.GetSpecialCharacter(esriSQLSpecialCharacters.esriSQL_WildcardManyMatch);
//            _DelimiterPost = sqlSyntx.GetSpecialCharacter(esriSQLSpecialCharacters.esriSQL_DelimitedIdentifierSuffix);
//            _DelimiterPre = sqlSyntx.GetSpecialCharacter(esriSQLSpecialCharacters.esriSQL_DelimitedIdentifierPrefix);
//        }
//        protected override bool SupportsSortingCore
//        {
//            get
//            {
//                return true;
//            }
//        }
//        protected override void RemoveSortCore()
//        {
//        }
//        protected override void ApplySortCore(System.ComponentModel.PropertyDescriptor prop, System.ComponentModel.ListSortDirection direction)
//        {
//            // MyBase.ApplySortCore(prop, direction)
//            try
//            {
//                // currently sorting will fail on tables that do not have an OBJECTID column
//                // perhaps even those not registered with a geodatbase
//                // check find filter first

//                if (_isSorted)
//                {
//                    if (_sortProperty.Name == prop.Name)
//                    {
//                        if (_sortDirection == ListSortDirection.Ascending)
//                            _sortDirection = ListSortDirection.Descending;
//                        else
//                            _sortDirection = ListSortDirection.Ascending;
//                    }
//                }
//                else
//                    _sortDirection = direction;

//                _sortProperty = prop;

//                Type fldType;
//                fldType = prop.PropertyType;
//                wrappedTableSort = new TableSortClass();
//                wrappedTableSort.Table = (ITable)wrappedTable;
//                wrappedTableSort.Fields = prop.Name;
//                if (_sortDirection == ListSortDirection.Descending)
//                {
//                    wrappedTableSort.Ascending[prop.Name]=false;
                    
//                        } else {
//                    wrappedTableSort.Ascending[prop.Name]= true;
//                }
//                base.Clear();
//                wrappedTableSort.Sort(new ESRI.ArcGIS.Carto.TrackCancel());
//                ICursor cur = wrappedTableSort.Rows;
//                IRow curRow = cur.NextRow();
//                while (curRow != null)
//                {
//                    base.Add(curRow);
//                    // Add(curRow)
//                    curRow = cur.NextRow();
//                }
//                _isSorted = true;
//            }
//            catch (Exception ex)
//            {
//               // logger.Log(NLog.LogLevel.Error, string.Format("Exception in {0} reads {1}", System.Reflection.Assembly.GetExecutingAssembly().GetModules()(0).Name, ex.Message));
//            }
//        }
//        protected override bool IsSortedCore
//        {
//            get
//            {
//                return _isSorted;
//            }
//        }
//        protected override System.ComponentModel.ListSortDirection SortDirectionCore
//        {
//            get
//            {
//                return _sortDirection;
//            }
//        }
//        public void ApplySort(System.ComponentModel.ListSortDescriptionCollection sorts)
//        {
//            throw new NotImplementedException();
//        }
//        private int PrepWhere(string FilterElements)
//        {
//            string[] sFilterParts;
//            //sFilterParts = Strings.Split(FilterElements, "|");
//            sFilterParts = FilterElements.Split('|');
//            string sFilterColumn;
//            string sFilterValue;
//            bool bString;

//            int indxFld;
//            sFilterColumn = sFilterParts[0];
//            sFilterValue = sFilterParts[1];
//            bString = System.Convert.ToBoolean(sFilterParts[2]);
//            indxFld = wrappedTable.FindField(sFilterColumn);
//            string sWhere;
//            PropertyDescriptorCollection fldProps = base.GetItemProperties(null/* TODO Change to default(_) if this is not a reference type */);
            
//            FieldPropertyDescriptor fldProp;
//            fldProp = fldProps.Find(sFilterColumn, false);
//            if (fldProp.PropertyType == typeof(string))
//            {
//                if (sFilterValue == "")
//                    sWhere = "(" + _DelimiterPre + sFilterColumn + _DelimiterPost + " = '' or " + _DelimiterPre + sFilterColumn + _DelimiterPost + " IS Null)";
//                else
//                    sWhere = _DelimiterPre + sFilterColumn + _DelimiterPost + " LIKE '" + _Wildcard + sFilterValue + _Wildcard + "'";
//            }
//            else if (sFilterValue == "NULL")
//                sWhere = sFilterColumn + " IS " + sFilterValue;
//            else if (sFilterValue.Substring(0, 2) == "IN")
//                sWhere = sFilterColumn + " (" + sFilterValue + ")";
//            else
//                sWhere = _DelimiterPre + sFilterColumn + _DelimiterPost + " = " + sFilterValue;
//            // if it was filtered before we'll add this filter
//            if (_isFiltered)
//                sWhere = _filterValue + " AND " + sWhere;
//            return DoSearch(sWhere);
//        }
//        private int DoSearch(string sWhere)
//        {
//            IQueryFilter2 pqf;
//            pqf = new QueryFilterClass();

//            // set the where clause for our query filter
//            pqf.WhereClause = sWhere;
//            ICursor cur;
//            cur = wrappedTable.Search(pqf, false);
//            IRow curRow = cur.NextRow();
//            int i = 0;
//            // if our where clause is successful then we'll clear the old table and reload
//            if (curRow != null)
//            {
//                base.Clear();
//                _filterValue = sWhere;
//                _isFiltered = true;
//            }
//            // reload
//            while (curRow != null)
//            {
//                i++;
//                Add(curRow);
//                curRow = cur.NextRow();
//            }
//            // returns the number of records 
//            return i;
//        }


//        protected override bool SupportsSearchingCore
//        {
//            get
//            {
//                return true;
//            }
//        }
//        protected override int FindCore(System.ComponentModel.PropertyDescriptor prop, object key)
//        {
//            // Return MyBase.FindCore(prop, key)
//            int i;
//            i = FindCore(_lastFoundIndx, prop, key);
//            if (i == _lastFoundIndx)
//            {
                
//                MessageBox.Show("Didn't find what you're looking for.","Find Error",MessageBoxButtons.OK);
//                _lastFoundIndx = 0;
//            }
//            return i;
//        }

//        protected new int FindCore(int startIndex, PropertyDescriptor prop, object key)
//        {
//            // propinfo is nothing
//            // Dim propInfo As PropertyInfo = GetType(IRow).GetProperty(prop.Name)
//            IRow r;
//            ICursor cur;
//            int indx;
//            int i=0;
//            string KeyToCheck;
//            cur = wrappedTable.Search(null/* TODO Change to default(_) if this is not a reference type */, false);
//            r = cur.NextRow();
//            indx = r.Fields.FindField(prop.Name);
//            string sKey;
//            if ((string)key != null)
//            {
//                sKey = System.Convert.ToString(key).ToLower();
//                while (r != null)
//                {
//                    if (i > _lastFoundIndx)
//                    {
//                        // find works for text not integer
//                        KeyToCheck = r.Value[indx].ToString();
//                        if ((string)KeyToCheck != null)
//                        {
//                            if (System.Convert.ToString(KeyToCheck).ToLower().Contains(sKey))
//                            {
//                                _lastFoundIndx = i;
//                                return i;
//                            }
//                        }
//                    }
//                    i = i + 1;


//                    r = cur.NextRow();
//                }
//            }


//            return -1;
//        }




//        public string Filter
//        {
//            get
//            {
//                return _filterValue;
//            }
//            set
//            {
//                if (PrepWhere(value) == 0)
//                    MessageBox.Show("Filter didn't match any records.","Find Problem",MessageBoxButtons.OK);
//            }
//        }


//        public void RemoveFilter()
//        {
//            DoSearch(null);
//            _isFiltered = false;
//        }

//        public System.ComponentModel.ListSortDescriptionCollection SortDescriptions
//        {
//            get
//            {
//                return null;
//            }
//        }

//        public bool SupportsAdvancedSorting
//        {
//            get
//            {
//                return false;
//            }
//        }

//        public bool SupportsFiltering
//        {
//            get
//            {
//                return true;
//            }
//        }
//    }

//}
