﻿using ESRI.ArcGIS.Desktop.AddIns;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGELibraryAddin
{
    public partial class RemoveDuplicate : Form
    {
        public RemoveDuplicate()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ESRI.ArcGIS.esriSystem.UID dockWinUID = ThisAddIn.IDs.DockPGEData.ToUID();
            ESRI.ArcGIS.Framework.IDockableWindow dockWin;
            DockPGEData dockPGEData= AddIn.FromID<DockPGEData.AddinImpl>(ThisAddIn.IDs.DockPGEData).UI;

            //            dockWin = ArcMap.DockableWindowManager.GetDockableWindow(dockWinUID);
            //DockPGEData.AddinImpl addinImpl = 
            //dockPGEData = (DockPGEData)dockWin;
            BatchData bd = dockPGEData.GetBatchData;
            if (bd.VersionName() == "SDE.Default" | string.IsNullOrEmpty(bd.VersionName()))
            {
                MessageBox.Show("Please create a version before using this tool");
                return;
            }
            if (!(cboType.Text != null && cboType.Text != ""))
            {
                MessageBox.Show("Please select the type of record to delete");
            }
            if (!(textDeleteID.Text != null && textDeleteID.Text != ""))
            {
                MessageBox.Show("Please enter a value to delete");
            }
            if (!(textKeepID.Text != null && textKeepID.Text != ""))
            {
                MessageBox.Show("Please enter a value for the replacement ID");
            }
            if (cboType.Text == "Resources")
            {
                bd.DeleteDuplicateResource(System.Convert.ToInt32(textDeleteID.Text), System.Convert.ToInt32(textKeepID.Text), checkDeleteGIS.Checked, checkAddToBatch.Checked);
            } else
            {
                bd.DeleteDuplicateReport(System.Convert.ToInt32(textDeleteID.Text), System.Convert.ToInt32(textKeepID.Text), checkDeleteGIS.Checked, checkAddToBatch.Checked);
            }

        }
    }
}
