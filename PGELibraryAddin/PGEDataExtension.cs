﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.ArcMapUI;

namespace PGELibraryAddin
{
    public class PGEDataExtension : ESRI.ArcGIS.Desktop.AddIns.Extension
    {
        private bool _batchDataEnabled;
           
        public PGEDataExtension()
        {
        }

        protected override void OnStartup()
        {
        }
        
        protected override void OnShutdown()
        {
        }
        public bool BatchDataEnabled
        {
            get { return _batchDataEnabled; }
            set { _batchDataEnabled = value; }
        }
    }

}
