﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGELibraryAddin
{
    public class MyConverter : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            // you need to get the list of values from somewhere
            // in this sample, I get it from the MyClass itself
            var myClass = context.Instance as EditableSettings;
            if (myClass != null)
                return new StandardValuesCollection(myClass.LayerNames);

            return base.GetStandardValues(context);
        }
    }
    class EditableSettings
    {
        FeatureLayerList _fl;
        List<string> _batchLayerNames;
        List<string> _BatchReportLayerNames;
        List<string> _BatchResourceLayerNames;
        public EditableSettings()
        {
            _fl = new FeatureLayerList();
            _batchLayerNames = new List<string>();
            GenericCode genericCode = new GenericCode();
            _fl = genericCode.GetLayers();

        }
        [Browsable(false)]
        public List<string> LayerNames
        {
            get { return _fl.NameList.ToList(); }
        }
        public List<string> BatchReportLayerNames {
            get
            {
                 string[] settingValues = { PGELibraryAddin.Properties.Settings.Default.BatchReportPoly,
                    PGELibraryAddin.Properties.Settings.Default.BatchReportLinear ,
                    PGELibraryAddin.Properties.Settings.Default.BatchReportPoint};
                _BatchReportLayerNames = new List<string>(settingValues);
                return _BatchReportLayerNames;
            }
        }
        public List<string> BatchResourceLayerNames
        {
            get
            {
                string[] settingValues = { PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly,
                    PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear ,
                    PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint};
                _BatchResourceLayerNames= new List<string>(settingValues);
                return _BatchResourceLayerNames;
            }
        }
        public List<string> BatchLayerNames
        {
            get {

                string[] settingValues = { PGELibraryAddin.Properties.Settings.Default.BatchReportPoly,
                    PGELibraryAddin.Properties.Settings.Default.BatchReportLinear ,
                    PGELibraryAddin.Properties.Settings.Default.BatchReportPoint,
                    PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear,
                    PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint,
                    PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly};
                _batchLayerNames = new List<string>(settingValues);
                return _batchLayerNames;
            }
        }

        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Report Polygons")]
        [System.ComponentModel.DisplayName("Report Polygons")]
        [TypeConverter(typeof(MyConverter))]
        public string ReportPoly
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchReportPoly;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchReportPoly = value;
            }
        }
        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Report Lines")]
        [System.ComponentModel.DisplayName("Report Lines")]
        [TypeConverter(typeof(MyConverter))]
        public string ReportLines
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchReportLinear;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchReportLinear = value;
            }
        }
        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Report Points")]
        [System.ComponentModel.DisplayName("Report Points")]
        [TypeConverter(typeof(MyConverter))]
        public string ReportPoints
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchReportPoint;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchReportPoint = value;
            }
        }
        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Resource Polygons")]
        [System.ComponentModel.DisplayName("Resource Polygons")]
        [TypeConverter(typeof(MyConverter))]
        public string ResourcePoly
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchResourcePoly = value;
            }
        }
        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Resource Lines")]
        [System.ComponentModel.DisplayName("Resource Lines")]
        [TypeConverter(typeof(MyConverter))]
        public string ResourceLines
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchResourceLinear = value;
            }
        }
        [System.ComponentModel.Category("Batch Layer Names")]
        [System.ComponentModel.Description("Resource Points")]
        [System.ComponentModel.DisplayName("Resource Points")]
        [TypeConverter(typeof(MyConverter))]
        public string ResourcePoints
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.BatchResourcePoint = value;
            }
        }
        [System.ComponentModel.Category("General")]
        [System.ComponentModel.Description("Location of PDF repository")]
        [System.ComponentModel.DisplayName("PDF Source")]
        [TypeConverter(typeof(MyConverter))]
        public string PDFSource
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.PDFSource;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.PDFSource = value;
            }
        }
        [System.ComponentModel.Category("General")]
        [System.ComponentModel.Description("Resource Visible Table Fields")]
        [System.ComponentModel.DisplayName("MPE Res Visible Fields")]
        [TypeConverter(typeof(MyConverter))]
        public StringCollection MPEResourceFieldsVisible
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceVisibleFields;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceVisibleFields = value;
            }
        }
        [System.ComponentModel.Category("General")]
        [System.ComponentModel.Description("Resource Order Table Fields")]
        [System.ComponentModel.DisplayName("MPE Res Order Fields")]
        [TypeConverter(typeof(MyConverter))]
        public StringCollection MPEResourceFieldsOrder
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnOrder;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnOrder = value;
            }
        }
        [System.ComponentModel.Category("General")]
        [System.ComponentModel.Description("Resource Hearder Fields")]
        [System.ComponentModel.DisplayName("MPE Res Header Fields")]
        [TypeConverter(typeof(MyConverter))]
        public StringCollection MPEResourceFieldsHeader
        {
            get
            {
                return PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnHeader;
            }
            set
            {
                PGELibraryAddin.Properties.Settings.Default.MPEResourceColumnHeader = value;
            }
        }
    }
}
