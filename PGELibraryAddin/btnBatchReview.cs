﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.Desktop.AddIns;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.ArcMapUI;

namespace PGELibraryAddin
{
    public class btnBatchReview : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public btnBatchReview()
        {
        }

        protected override void OnClick()
        {
            ESRI.ArcGIS.esriSystem.UID dockWinUID = ThisAddIn.IDs.DockPGEData.ToUID();
            ESRI.ArcGIS.Framework.IDockableWindow dockWin;
            DockPGEData dockPGEData = AddIn.FromID<DockPGEData.AddinImpl>(ThisAddIn.IDs.DockPGEData).UI;

            BatchData bd = dockPGEData.GetBatchData;
            GenericCode WorkerBee = new GenericCode();
            IStandaloneTable batchTable = WorkerBee.GetStandAloneTable("pgelibrary.geo.batch");
            ITableSelection batchTableSelection;
            ITableSelection reportTableSelection;
            ITableSelection resourceTableSelection;
            ICursor cur = null;
            IRow row;
            batchTableSelection = (ITableSelection)batchTable;
            string batchID;
            if (batchTableSelection.SelectionSet != null & bd.Enabled)
            {
                if (batchTableSelection.SelectionSet.Count == 1)
                {
                    batchTableSelection.SelectionSet.Search(null, false, out cur);
                    row = cur.NextRow();
                    //batchID = cur.Fields.Field[cur.FindField("globalid")]
                    batchID = (string)row.get_Value(row.Fields.FindField("globalid"));
                    IStandaloneTable standaloneTableReport = WorkerBee.GetStandAloneTable("pgelibrary.geo.batchreport");
                    IStandaloneTable standaloneTableResource = WorkerBee.GetStandAloneTable("pgelibrary.geo.batchresource");
                    IQueryFilter2 qf = new QueryFilterClass();
                    qf.WhereClause = "batchid ='" + batchID + "'";
                    reportTableSelection = (ITableSelection)standaloneTableReport;
                    reportTableSelection.SelectRows(qf, esriSelectionResultEnum.esriSelectionResultNew, false);
                    resourceTableSelection = (ITableSelection)standaloneTableResource;
                    resourceTableSelection.SelectRows(qf, esriSelectionResultEnum.esriSelectionResultNew, false);

                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Please select one row in the batch table");
            }
        }

        protected override void OnUpdate()
        {
        }
    }
}
