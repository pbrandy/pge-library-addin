﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace PGELibraryAddin
{
    public class btnRemoveDuplicate : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        RemoveDuplicate f;
        public btnRemoveDuplicate()
        {
          }

        protected override void OnClick()
        {
            f = new RemoveDuplicate();
            f.Show(System.Windows.Forms.Control.FromHandle((IntPtr)ArcMap.Application.hWnd));

        }

        protected override void OnUpdate()
        {
        }
    }
}
