﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PGELibraryAddin
{
    public class toolShowPGEData : ESRI.ArcGIS.Desktop.AddIns.Tool
    {
        public toolShowPGEData()
        {
        }

        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }
    }

}
