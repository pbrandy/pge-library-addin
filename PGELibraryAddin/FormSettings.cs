﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGELibraryAddin
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
        }
        
        private void FormSettings_Load(object sender, EventArgs e)
        {
            propertyGrid1.SelectedObject = new EditableSettings();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            PGELibraryAddin.Properties.Settings.Default.Save();
            this.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
