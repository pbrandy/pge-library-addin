﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace PGELibraryAddin
{
    public class btnShowDockWindow : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public btnShowDockWindow()
        {
           
        }


        protected override void OnClick()
        {

            ESRI.ArcGIS.esriSystem.UID dockWinUID = ThisAddIn.IDs.DockPGEData.ToUID();
            ESRI.ArcGIS.Framework.IDockableWindow dockWin;
            DockPGEData dockPGEData;
            dockWin = ArcMap.DockableWindowManager.GetDockableWindow(dockWinUID);
            if (dockWin.IsVisible()) {
                dockWin.Show(false);
            } else {
                dockWin.Show(true);
            }


            ArcMap.Application.CurrentTool = null;
        }
        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
            
        }
    }

}
